#!/bin/bash

#Update System
echo "[*] Updating the system!"
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y

#Get needed libs
echo "[*] Install dependencies!"
wget https://www.infradead.org/~tgr/libnl/files/libnl-1.1.4.tar.gz
tar -zxpvf libnl-1.1.4.tar.gz
cd libnl-1.1.4
./configure
make
make install
cd ..

sudo apt-get install libssl-dev libnl-utils -y

#Install programm
echo "[*] Install hostapd-wpe!"
wget http://w1.fi/releases/hostapd-2.2.tar.gz
tar -zxf hostapd-2.2.tar.gz
git clone https://github.com/OpenSecurityResearch/hostapd-wpe.git
cd hostapd-2.2/
patch -p1 < ../hostapd-wpe/hostapd-wpe.patch
cd hostapd
make
cd ../../hostapd-wpe/certs
./bootstrap
cd ../..

#Clean up
echo "[*] Cleaning up!"
rm -r hostapd-2.2.tar.gz libnl-1.1.4.tar.gz libnl-1.1.4

#Ready
echo "[*] Installation is completed!"
echo "[*] All the important stuff is in hostapd-2.2/hostapd/"
echo "[*] Please configure hostapd-wpe.conf"
echo "[*] Usgae: ./hostapd-wpe hostapd-wpe.conf"
echo "[*] *** Have FUN ***"
